# alpine-nginx
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-nginx)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-nginx)



----------------------------------------
### x64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/alpine-nginx/x64)
### aarch64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/alpine-nginx/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [nginx](http://nginx.org/)
    - nginx [engine x] is an HTTP and reverse proxy server, a mail proxy server, and a generic TCP/UDP proxy server, originally written by Igor Sysoev.



----------------------------------------
#### Run

```sh
docker run -d \
           -p 80:80/tcp \
           -p 443:443/tcp \
           -v /conf.d:/conf.d \
           forumi0721/alpine-nginx:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:80/](http://localhost:80/)
    - If you want to use custom setting, you need to create `nginx.conf` and add `-v nginx.conf:/etc/nginx/nginc.conf` to docker option.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 80/tcp             | HTTP port                                        |
| 443/tcp            | HTTPS port                                       |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

